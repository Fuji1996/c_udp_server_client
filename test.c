#include <stdio.h>
#include <stdlib.h> 
#include <time.h>

int main(){
    printf("hello world");
    struct timespec tstart={0,0}, tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);
    for(int i=0;i<10000;i++){
        
    }
    clock_gettime(CLOCK_MONOTONIC, &tend);
    printf("some_long_computation took about %.5f nano seconds\n",
           ((double)1.0e9*tend.tv_sec + (double)tend.tv_nsec) - 
           ((double)1.0e9*tstart.tv_sec + (double)tstart.tv_nsec));
    return 0;
}